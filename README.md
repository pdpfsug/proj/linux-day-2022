# Linux Day 2022 - Undefined ##

![][locandina]

## Luogo e orario ##

* F-actory (Via Vittorio Veneto, 50, 60044 Fabriano AN), Sala superiore
* Sabato 22 ottobre, 15:00 - 19:00

## Prestazione per il giornale ##

## Programma ##

## Bozza programma ##

PS: Interruzione dei workshop durante i talk

* 15:00 - Talk d'introduzione
* 15:15 - Workshop
* 16:45 - Fairphone
* 17:00 - Rinfresco
* 17:30 - Talk Web3
* 17:45 - AI & Stop problem
* 18:00 - Ripresa workshop
* 19:00 - Chiusura

### Workshop ###

* Blender e disegno virtuale (Andrei)
* Python (Dawid)
* Alternative Opensource e sicurezza (Costin)
* Rinfresco

### Talk ###

* Discorso d'introduzione (?)
* Fairphone (Lorenzo Armezzani)
* Talk Web3 (Costin)
* Button-problem (Raffaele)
* Discorso di chiusura (?)

## Tasks ##

Andrei:
    - Preparare workshop
    - Accettazione iscrizioni

Azzurra:
    - Locandina

Costin:
    - Preparare talk Web3
    - Preparere le attivita' del proprio workshop
    - Registazione talk
    - Preparazione QR dei vari links

Dawid:
    - Carica registrazione su YT
    - Articolo sul sito
    - Pubblicazione programma + Locandina
    - Comprare cibo
    - Recuperare moduli iscrizione

Tutti:
    - Preparare sala grande (almeno 1 ora prima)

[locandina]: ./locandina_ld2022.png
